import requests

from requests.auth import HTTPBasicAuth, AuthBase


class BaseClient():

    def _init_auth(self, auth):
        if auth:
            if isinstance(auth, AuthBase):
                return auth
            try:
                if auth["auth_type"] == "basic":
                    return HTTPBasicAuth(auth["username"], auth["password"])
                else:
                    raise ValueError("unsupported auth_type "
                                     f"{auth['auth_type']}")
            except KeyError:
                raise ValueError("auth must be either an "
                                 "instance of requests.auth or a marshalled "
                                 "PiperCI dict")
        else:
            return None

    def _make_request(self, request_type, uri, data=None):
        try:
            request = getattr(requests, request_type)
            response = request(f"{self.url}{uri}", json=data, auth=self.auth)
        except requests.exceptions.RequestException as e:
            raise requests.exceptions.RequestException(
                f"Failed to make request. \n\n{e}"
            )
        if response.status_code != 200 and response.status_code != 404:
            raise requests.exceptions.HTTPError(
                f"Received unexpected HTTP status code {response.status_code} "
                f"when requesting {self.url}{uri}\n\n"
            )
        return response

    def get(self, uri, data=None, raw=False):
        if raw:
            return self._make_request("get", uri, data=data)
        else:
            return self._make_request("get", uri, data=data).json()

    def head(self, uri):
        return self._make_request("head", uri)

    def post(self, uri, data=None):
        return self._make_request("post", uri, data=data).json()

    def put(self, uri, data=None):
        return self._make_request("put", uri, data=data).json()
