from abc import abstractmethod
import logging
import time

from piperci.gman.exceptions import TaskError
from piperci.base.client import BaseClient


log = logging.getLogger(__name__)


class TaskManagerClient(BaseClient):
    @abstractmethod
    def new_task(
        self,
        run_id,
        project,
        caller,
        status,
        thread_id=None,
        parent_id=None,
        artifact_id=None,
    ):
        """Request a new task to be associated with a given run_id"""

    @abstractmethod
    def get_events(self):
        """
        Get list of all events for a given thread_id, run_id, or task_id, optionally
        filtered by a lambda expression.
        """

    @abstractmethod
    def get_health(self):
        """
        Gets the status of the GMan server's /health endpint.
        """

    @abstractmethod
    def get_run(self, run_id):
        """
        Gets a run from Gman if it exists. If it does not exist then we return
        an empty list
        """

    @abstractmethod
    def get_task(self, task_id):
        """
        Gets a task data given a task ID. Returns an empty dict if the task doesn't exist
        :param task_id: task_id to query as a string
        """

    @abstractmethod
    def get_thread(self, thread_id):
        """
        Gets a thread from Gman if it exists. If it does not exist then we return
        an empty list.
        """

    @abstractmethod
    def get_thread_tasks(self, thread_id, query_filter):
        """
        Get a list of tasks associated with the given thread_id
        """

    @abstractmethod
    def update_task(
        self, *, task_id, message, status, artifact_id=None, return_code=None
    ):
        """
        Updates a taskID status and/or message
        """

    @abstractmethod
    def wait_for_task(self, task_id, status, retry_max=10):
        """
        Returns true if given task_id has a status of the given status. If retry_max
        is reached without a matching status then a Timeout exception is raised. If
        the given task returns with a "failed" state then we raise a TaskError exception.
        """

    @abstractmethod
    def wait_for_thread(self):
        """
        Wait for all tasks under a given thread_id to return with a status of complete,
        up to the retry_max.
        """


class GmanClient(TaskManagerClient):
    """
    Gman client class which implements the TaskManagerClient abstract base class.
    This library is the primary interface for interacting with the Task Manager.
    """

    def __init__(self, url, auth=None):
        """
        :param url: The URL of the Gman instance
        :param auth: Authentication object to pass along to requests. Must be of
        type `requests.auth.AuthBase`
        """
        self.url = url
        self.auth = self._init_auth(auth)

    def new_task(
        self,
        *,
        run_id,
        project,
        caller,
        status,
        thread_id=None,
        parent_id=None,
    ):
        """
        Request a new TaskID from GMan, associated with a given RunID
        :param run_id: Unique identifier to correlate taskIDs as a string
        :param project: Name of your project as a string
        :param caller: The invoker of the task. as a string
        :param status: The initial status of the task. This must either be "started"
        or "received". Unless the caller of this function is an executor this should
        always be "started"
        :param: thread_id: The thread_id that this task should be associated with.
        This will mainly be used by executors who need to tie their task_id to the
        main task thread.
        :param: parent_id: The parent_id of the task which was delegated to this task.
        This is mainly used by executors who need to tie their task_id to the task
        of it's parent.
        :return: JSON resposne from GMan
        """
        if not status or (status != "started" and status != "received"):
            raise ValueError(
                f"Invalid status '{status}'. Must be 'received' or 'started'."
            )
        if status == "received" and (not thread_id or not parent_id):
            raise ValueError(f"thread_id must be specified if status is received.")
        log.debug(f"Requesting new taskID from gman at {self.url}")
        data = {
            "run_id": run_id,
            "caller": caller,
            "project": project,
            "message": "Requesting new taskID",
            "status": status,
        }
        if thread_id:
            data.update({"thread_id": thread_id})
        if parent_id:
            data.update({"parent_id": parent_id})
        return self.post("/task", data)

    def wait_for_task(self, *, task_id, status, retry_max=10):
        """
        Returns true if given task_id has a status of the given status. If retry_max
        is reached without a matching status then a Timeout exception is raised. If
        the given task returns with a "failed" state then we raise a TaskError exception.
        :param task_id: TaskID to query for as a string
        :param status: Status to wait for as a string
        :param retry_max: The number of times to retry the query as an integer
        :return: True or exception
        """
        retries = 0
        while retries < retry_max:
            log.debug(f"Checking status of task {task_id}")
            task_events = self.get(f"/task/{task_id}/events")
            events = [event for event in task_events if event.get("status") == status]
            events_failed = [
                event for event in task_events if event.get("status") == "failed"
            ]
            if len(events_failed):
                raise TaskError(
                    f"Task {task_id} has failed task events. {events_failed}"
                )
            if len(events):
                return True
            else:
                retries += 1
                time.sleep(1)

        raise TimeoutError(f"Checking task status timeout for task {task_id}")

    def update_task(
        self, *, task_id, message, status, artifact_id=None, return_code=None
    ):
        """
        Updates a taskID status and/or message
        :param task_id: TaskID to update as a string
        :param message: The message to apply to the task
        :param status: The status to apply to the task
        :param artifact: Associate an existing artifact with this event as a UUID
        :param return_code: Add a return code to this event as an int
        :return: JSON response from gman
        """
        data = {"message": message, "status": status}
        if artifact_id:
            data["artifact"] = artifact_id
        if return_code is not None:
            data["return_code"] = return_code
        return self.put(f"/task/{task_id}", data=data)

    def _get_task_events(self, *, task_id, query_filter=None):
        """
        Get a list of taskID events from gman.
        Optionally pass a query_fitler lamda expression to filter these events.
        For example, to return a list of all failed events for a particular taskID
        get_task_id_events(
          task_id='1234',
          query_filter=lambda x: x.get('status') == 'failed'
        )
        :param task_id: taskID to query as a string
        :param query_filter: lambda expression
        :return: List of events
        """
        response = self.get(f"/task/{task_id}/events")
        if query_filter:
            return list(filter(query_filter, response))
        else:
            return response

    def get_thread_tasks(self, *, thread_id, query_filter=None):
        """
        Get a list of tasks associated with the given thread_id
        :param thread_id: The thread_id to query with as a sring
        :param query_filter: lambda expression
        :return: List of tasks associated with thread_id
        """
        response = self.get(f"/thread/{thread_id}")
        if query_filter:
            return list(filter(query_filter, response))
        else:
            return response

    def _get_thread_events(self, *, thread_id, query_filter=None):
        """
        Get list of all events for a given thread_id, optionally filtered by
        a lambda expression.
        :param thread_id: Thread ID to query for as a string.
        :param query_filter: Lambda expression
        :return: List of task events
        """
        response = self.get(f"/thread/{thread_id}/events")
        if query_filter:
            return list(filter(query_filter, response))
        else:
            return response

    def wait_for_thread(self, *, thread_id, status="completed", retry_max=10):
        """
        Wait for all tasks under a given thread_id to return with a status of complete,
        up to the retry_max.
        :param thread_id: thread_id as a string to search for
        :param retry_max: Number of times to retry as an integer
        :param status: The status to wait for.
        :return: True or exception
        """
        retries = 0
        while retries < retry_max:
            log.debug(f"Checking status of thread {thread_id}")
            response = self.head(f"/thread/{thread_id}")
            running, completed, pending, failed = [
                response.headers.get(key)
                for key in [
                    "x-gman-tasks-running",
                    "x-gman-tasks-completed",
                    "x-gman-tasks-pending",
                    "x-gman-tasks-failed",
                ]
            ]
            if int(failed) > 0:
                raise TaskError(f"Thread {thread_id} has failures")
            elif int(running) > 0 or int(pending) > 0:
                retries += 1
                time.sleep(1)
            elif int(running) == 0 and int(completed) > 0 and int(pending) == 0:
                return True
            else:
                retries += 1
                time.sleep(1)
        raise TimeoutError(
            f"Checking thread_id status timed out for thread_id {thread_id}"
        )

    def get_events(
        self, *, thread_id=None, task_id=None, run_id=None, query_filter=None
    ):
        """
        Get list of all events for a given thread_id, run_id, or task_id, optionally
        filtered by a lambda expression.
        :param thread_id: (Optional) Thread ID to query for as a string.
        :param task_id: (Optional) Task ID to query for as a string.
        :param run_id: (Optional) Run ID to query for as a string.
        :param query_filter: Lambda expression
        :return: List of task events
        """
        mutually_exclusive_args = [arg for arg in [thread_id, run_id, task_id] if arg]
        if len(mutually_exclusive_args) > 1:
            raise ValueError(
                f"Multiple exclusive arguments given. You must pass exactly one of "
                f"thread_id, run_id, and task_id."
            )
        if thread_id:
            return self._get_thread_events(
                thread_id=thread_id, query_filter=query_filter
            )
        elif run_id:
            return self._get_run_events(run_id=run_id, query_filter=query_filter)
        elif task_id:
            return self._get_task_events(task_id=task_id, query_filter=query_filter)
        else:
            raise ValueError(f"One of thread_id, run_id, or task_id must be provided.")

    def _get_run_events(self, *, run_id=None, query_filter=None):
        """
        Get a list of run_id events from gman.
        Optionally pass a query_fitler lamda expression to filter these events.
        For example, to return a list of all failed events for a particular runID
        _get_run_id_events(
          run_id='1234', query_filter=lambda x: x.get('status') == 'failed'
        )
        :param run_id: run_id to query as a string
        :param query_filter: lambda expression
        :return: List of events
        """
        response = self.get(f"/run/{run_id}/events")
        if query_filter:
            return list(filter(query_filter, response))
        else:
            return response

    def get_task(self, *, task_id):
        """
        Gets a task data given a task ID. Returns an empty dict if the task doesn't exist
        :param task_id: task_id to query as a string
        :return: Task dictionary
        """
        response = self.get(f"/task/{task_id}", raw=True)

        if response.status_code == 404:
            return {}
        else:
            return response

    def get_run(self, *, run_id):
        """
        Gets a run from Gman if it exists. If it does not exist then we return
        an empty list
        :param run_id: run_id to query as a string
        :return: List of tasks in a run
        """
        response = self.get(f"/run/{run_id}", raw=True)

        if response.status_code == 404:
            return []
        else:
            return response

    def get_thread(self, *, thread_id):
        """
        Gets a thread from Gman if it exists. If it does not exist then we return
        an empty list.
        :param thread_id: thread_id as a string
        :return: List of tasks in a thread
        """
        response = self.get(f"/thread/{thread_id}", raw=True)

        if response.status_code == 404:
            return []
        else:
            return response

    def get_health(self):
        """
        Gets the status of the GMan server's /health endpint.
        """
        return self.get("/health")
