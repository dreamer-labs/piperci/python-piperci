import logging

from piperci.base.client import BaseClient


log = logging.getLogger(__name__)


class ArtmanClient(BaseClient):
    def __init__(self, url, auth=None):
        self.url = url
        self.auth = self._init_auth(auth)

    def _check_artifact_exists_for_sri(self, *, sri_urlsafe):
        log.debug(f"Checking if artifact record exists for SRI {sri_urlsafe}")
        response = self.head(f"/artifact/sri/{sri_urlsafe}")
        if response.status_code == 404:
            log.debug(f"Artifact with SRI {sri_urlsafe} does not exist")
            return False
        elif response.status_code == 200:
            log.debug(f"Artifact with SRI {sri_urlsafe} exists")
            return True

    def _check_artifact_exists_for_task_id(self, *, task_id):
        log.debug(f"Checking if artifact record exists for task_id {task_id}")
        response = self.head(f"/artifact/task/{task_id}")
        if response.status_code == 404:
            log.debug(f"Artifact with task_id {task_id} does not exists")
            return False
        elif response.status_code == 200:
            log.debug(f"Artifact with task_id {task_id} exists")
            return True

    def _get_artifact_by_artifact_id(self, artifact_id):
        log.debug(f"Getting artifact records for artifact_id {artifact_id}")
        return self.get(f"/artifact/{artifact_id}")

    def _get_artifacts_by_sri(self, *, sri_urlsafe, query_filter=None):
        log.debug(f"Getting artifact records for SRI {sri_urlsafe}")
        response = self.get(f"/artifact/sri/{sri_urlsafe}")
        log.debug(f"Received response from artman.\n{response}")
        if query_filter:
            return list(filter(query_filter, response))
        else:
            return response

    def _get_artifacts_by_task_id(self, *, task_id, query_filter=None):
        log.debug(f"Getting artifact records for task_id {task_id}")
        response = self.get(f"/artifact/task/{task_id}")
        log.debug(f"Received response from artman.\n{response}")
        if query_filter:
            return list(filter(query_filter, response))
        else:
            return response

    def _get_artifacts_by_thread_id(self, *, gman_client, thread_id, query_filter=None):
        log.debug(f"Getting artifact records for thread_id {thread_id}")
        task_ids = [
            task.get("task_id")
            for task in gman_client.get_thread_tasks(thread_id=thread_id)
        ]
        artifacts = [
            artifact
            for task_id in task_ids
            for artifact in self._get_artifacts_by_task_id(task_id=task_id)
        ]
        if query_filter:
            return list(filter(query_filter, artifacts))
        else:
            return artifacts

    @classmethod
    def artifact_types(cls):
        return ["stdout", "stderr", "log", "artifact"]

    def check_artifact_exists(self, *, sri_urlsafe=None, task_id=None):
        """
        Checks artman to see if an SRI has already been uploaded.
        :param task_id: The task_id to query for as a string
        :param sri_urlsafe: the SRI hash to query for as a string. Must be URL-safe.
        :return: True if artifacts exist for the query, False if they don't.
        """
        mutually_exclusive_args = [arg for arg in [sri_urlsafe, task_id] if arg]
        if len(mutually_exclusive_args) > 1:
            raise ValueError(
                f"Multiple exclusive arguments given. You must pass exactly one of "
                f"sri_urlsafe and task_id."
            )
        if sri_urlsafe:
            return self._check_artifact_exists_for_sri(sri_urlsafe=sri_urlsafe)
        elif task_id:
            return self._check_artifact_exists_for_task_id(task_id=task_id)
        else:
            raise ValueError(
                f"One of sri_urlsafe, artifact_id, or task_id must be provided."
            )

    def check_artifact_status(self, artifact_id):
        """
        Checks the status of an artifact
        :param artifact_id: Artifact ID to query for
        :return:
        """
        log.debug(f"Checking artifact status for artifact_id {artifact_id}")
        response = self.head(f"/artifact/{artifact_id}")
        return response.headers["x-gman-artifact-status"]

    def get_artifact(
        self,
        *,
        gman_client,
        sri_urlsafe=None,
        artifact_id=None,
        task_id=None,
        thread_id=None,
        query_filter=None,
    ):
        """
        Gets metadata about an artifact. Only one of [sri_urlsafe, artifact_id, task_id]
        may be passed.
        :param sri_urlsafe: URL-safe SRI of the artifact as a string
        :param artifact_id: ID of the artifact
        :param task_id: TaskID to query for
        :param thread_id: ThreadID to query for
        :param query_filter: A lambda expression to filter the results
        :return: List of artifact objects
        """
        mutually_exclusive_args = [
            arg for arg in [sri_urlsafe, artifact_id, task_id, thread_id] if arg
        ]
        if len(mutually_exclusive_args) > 1:
            raise ValueError(
                f"Multiple exclusive arguments given. You must pass exactly one of "
                f"sri_urlsafe, artifact_id, thread_id, and task_id."
            )

        if sri_urlsafe:
            return self._get_artifacts_by_sri(
                sri_urlsafe=sri_urlsafe, query_filter=query_filter
            )
        elif artifact_id:
            return [self._get_artifact_by_artifact_id(artifact_id=artifact_id)]
        elif task_id:
            return self._get_artifacts_by_task_id(
                task_id=task_id, query_filter=query_filter
            )
        elif thread_id:
            return self._get_artifacts_by_thread_id(
                gman_client=gman_client, thread_id=thread_id, query_filter=query_filter
            )
        else:
            raise ValueError(
                f"One of sri_urlsafe, artifact_id, or task_id must be provided."
            )

    def create_artifact(self, *, task_id, uri, type="artifact", sri):
        """
        Create a new artifact in ArtMan with the given parameters.
        :param task_id: The ID of the task to tie the artifact to.
        :param storage_uri: The URI that the artifact is located at
        :param type: The type. This is usually artifact.
        :param sri: The SRI hash of the artifact
        :return: JSON response from ArtMan if the artifact doesn't exist. If the artifact
        does exist then we throw an exception.
        """
        log.debug(f"Creating new artifact record at {self.url}")
        data = {
            "task_id": task_id,
            "sri": sri,
            "uri": uri,
            "type": type,
        }
        return self.post(f"/artifact", data=data)
