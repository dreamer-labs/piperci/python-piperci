import requests.exceptions


class ArtmanHTTPError(requests.exceptions.HTTPError):
    pass


class ArtmanRequestException(requests.exceptions.RequestException):
    pass
