from marshmallow import fields, Schema, RAISE, validate

from piperci.marshaller.task import validate_sri


class TaskManagerCreateTask(Schema):
    """A simplified PiperCI TaskManager compliant create task schema"""
    project = fields.Str(required=True)
    thread_id = fields.UUID(required=False,
                            default=None,
                            missing=None)
    parent_id = fields.UUID(required=False)
    run_id = fields.Str(required=True)
    caller = fields.Str(required=True)
    status = fields.Str(required=True)
    message = fields.Str(required=True)

    class Meta:
        unknown = RAISE


class TaskManagerUpdateTask(Schema):
    """A simplified PiperCI TaskManager compliant update task schema"""
    project = fields.Str()
    thread_id = fields.UUID()
    parent_id = fields.UUID()
    run_id = fields.Str()
    caller = fields.Str()
    status = fields.Str()
    message = fields.Str()
    artifact = fields.UUID()
    return_code = fields.Int()


class TaskManagerCreateArtifact(Schema):
    """A simplified PiperCI TaskManager compliant create artifact schema"""
    type = fields.Str(required=True,
                      validate=validate.OneOf(["log",
                                               "stdout",
                                               "stderr",
                                               "container",
                                               "artifact",
                                               "source"]))

    task_id = fields.UUID(required=True)
    sri = fields.Str(required=True,
                     validate=validate_sri)
    uri = fields.Url(
        require_tld=True,
        required=True,
        schemes=["http", "https", "ftp"],
        relative=False
    )

    class Meta:
        unknown = RAISE
