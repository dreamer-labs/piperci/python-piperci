from marshmallow_polyfield import PolyField


class PolyMutableField(PolyField):

    def deserialization_schema_selector(self, value, obj):
        return self._deserialization_schema_selector_arg(
            value, obj, **self.metadata)

    def serialization_schema_selector(self, value, obj):
        return self._serialization_schema_selector_arg(
            value, obj, **self.metadata)
