from marshmallow import (fields, validate, Schema,
                         ValidationError, RAISE)


def serialize_auth(base_object, parent_obj):
    try:
        if not isinstance(base_object, dict):
            key = base_object.auth_type  # pragma: no cover
        else:
            key = base_object["auth_type"]
        return auth_types[key]()
    except KeyError as e:
        key = str(e).strip("'")
        if key == "auth_type":
            raise ValidationError(
                'missing "auth_type" field'
                f" should be one of {str(auth_types.keys())}")
        else:
            raise ValidationError(
                f'invalid value {str(key)} for field "auth_type"'
                f" should be one of {str(auth_types.keys())}")


def deserialize_auth(object_dict, parent_object_dict):

    try:
        return auth_types[object_dict["auth_type"]]
    except KeyError as e:
        key = str(e).strip("'")
        if key == "auth_type":
            raise ValidationError(
                'missing "auth_type" field'
                f" should be one of {str(auth_types.keys())}")
        else:
            raise ValidationError(
                f'invalid value {str(key)} for field "auth_type"'
                f" should be one of {str(auth_types.keys())}")


class BasicAuthSchema(Schema):

    class Meta:
        unknown = RAISE

    auth_type = fields.Str(validate=validate.OneOf(["basic"]))

    username = fields.Str()
    password = fields.Str()


class BearerAuthSchema(Schema):

    class Meta:
        unknown = RAISE

    auth_type = fields.Str(validate=validate.OneOf(["bearer"]))

    token = fields.Str()


auth_types = {
    "basic": BasicAuthSchema,
    "bearer": BearerAuthSchema
}
