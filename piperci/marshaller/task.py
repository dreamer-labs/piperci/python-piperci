from typing import Any, Pattern, Type, Union

from marshmallow import (fields, Schema,
                         ValidationError, EXCLUDE)

from piperci.marshaller.fields import PolyMutableField

from piperci.marshaller.service import deserialize_service, serialize_service
from piperci.sri import sri_to_hash


def validate_sri(sri):
    try:
        sri_to_hash(sri)
    except Exception:
        raise ValidationError("Not a valid SRI")


class TaskInitSchema(Schema):
    """Schema for a Task execution call.
        Implements interface defined in the apispec for a PiperCI Task.
    """
    class Meta:
        unknown = EXCLUDE

    def __init__(self,
                 config_schema: Type[Schema],
                 endpoint_regexes: Union[Pattern, str, dict],
                 **kwargs: Any) -> None:

        self._declared_fields["config"] = fields.Nested(
            config_schema,
            required=True,
            help="PiperCI Task specific config options")

        self._declared_fields["service_configs"] = fields.List(
            PolyMutableField(
                serialization_schema_selector=serialize_service,
                deserialization_schema_selector=deserialize_service,
                endpoint_regexes=endpoint_regexes,
                **kwargs),
            required=True,
            help="service config dictionary polymorphpic based on \"service\"")

        super().__init__()

    artifacts = fields.List(fields.Str(validate=validate_sri),
                            required=False,
                            missing=[],
                            default=[],
                            help="artifacts list, item must be a valid SRI")

    project = fields.Str(required=True,
                         help="project name")

    stage = fields.Str(required=True,
                       help="stage id/name")
    thread_id = fields.UUID(required=False,
                            default=None,
                            missing=None,
                            help="a valid PiperCI Task thread_id")
    parent_id = fields.UUID(required=True,
                            help="a valid PiperCI Parent task_id")
    run_id = fields.Str(required=True,
                        help="a run id free form string")
    only_validate = fields.Boolean(required=False,
                                   default=False,
                                   missing=False,
                                   help="boolean toggles Task to only validate")
