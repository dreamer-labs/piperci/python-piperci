import re
from typing import List, Pattern, Type, Union

from marshmallow import (fields, validate, validates,
                         validates_schema, Schema,
                         ValidationError, RAISE)

from marshmallow_polyfield import PolyField

from piperci.marshaller.auth import deserialize_auth, serialize_auth


def prep_regex(endpoint_pattern: Union[str, Pattern]):

    if isinstance(endpoint_pattern, re.Pattern):
        return endpoint_pattern
    else:
        return re.compile(endpoint_pattern)


def serialize_service(base_object,
                      parent_obj,
                      **kwargs):

    try:
        if not isinstance(base_object, dict):  # pragma: no cover
            key = base_object.service
        else:
            key = base_object["service"]

        return service_types[key](**kwargs)
    except (KeyError, AttributeError) as e:
        err_key = str(e).strip("'")
        if err_key == "service":
            raise ValidationError(
                'missing "service" field'
                f" should be one of {str(service_types.keys())}")
        else:
            raise ValidationError(
                f'invalid value {err_key} for field "service"'
                f" should be one of {str(service_types.keys())}")


def deserialize_service(object_dict,
                        parent_object_dict,
                        **kwargs):

    try:
        return service_types[object_dict["service"]](**kwargs)
    except KeyError as e:
        err_key = str(e).strip("'")
        if err_key == "service":
            raise ValidationError(
                'missing "service" field'
                f" should be one of {str(service_types.keys())}")
        else:
            raise ValidationError(
                f'invalid value {err_key} for field "service"'
                f" should be one of {str(service_types.keys())}")


class ServiceDefinitionSchema(Schema):
    """Schema for a defined service backend.
    """
    class Meta:
        unknown = RAISE

    def __init__(self,
                 endpoint_regexes: Union[dict, str, Pattern],
                 allowed_backends: list = None,
                 force_https: bool = True,
                 **kwargs: str) -> None:

        if allowed_backends:
            self.allowed_backends = allowed_backends
        else:
            self.allowed_backends = self.def_allowed_backends

        if isinstance(endpoint_regexes, dict):
            self.endpoint_regexes = {}
            # passed in a dict of backends as keys mapped a regex pattern
            for backend, regex in endpoint_regexes.items():
                # only load backend regexes for current classes
                # supported backend types
                if (backend == "default"
                        or backend in self.allowed_backends):
                    self.endpoint_regexes[backend] = prep_regex(
                        endpoint_regexes[backend]
                    )
        else:
            # passed in a default backend regex
            # string generic to all backend types.
            self.endpoint_regexes = {"default": prep_regex(endpoint_regexes)}

        allowed_endpoint_schemes = ["https"]

        if not force_https:
            allowed_endpoint_schemes.append("http")

        self._declared_fields["endpoints"] = fields.List(
            fields.Nested(EndpointDefinitionSchema(self,
                                                   allowed_endpoint_schemes)),
            help="Endpoint definitions used to define how to connect "
                 "to required backend endpoints"
        )

        self._declared_fields["service"] = fields.Str(
            validate=validate.OneOf(service_types.keys()),
            help=f"Service type, must be one of {str(service_types.keys())}")

        super().__init__()

    # used to provide informative responses}
    supported_backends = fields.List(fields.Str())
    supported_auth = fields.List(fields.Str())

    @property
    def default_backend(self):
        if self.allowed_backends:
            return self.allowed_backends[0]


class TaskManagerServiceSchema(ServiceDefinitionSchema):
    """Task manager service Schema.
    """
    def_allowed_backends = ["gman"]
    backends_limit = 1


class ArtifactStoreSchema(ServiceDefinitionSchema):
    """Artifact store service schema.
    """
    def_allowed_backends = ["minio"]
    backends_limit = 1


class PiperCITaskSchema(ServiceDefinitionSchema):
    """PiperCI task service definition.
    """
    def_allowed_backends = ["piperci"]
    backends_limit = 1

    def __init__(self, **kwargs):

        self.enabled_subtasks = kwargs.get("enabled_subtasks", [])

        self._declared_fields["sub_task"] = fields.Str(
            help="subtask key must/may be one of "
                 f"{str(self.enabled_subtasks)}"
        )

        super().__init__(**kwargs)

    @validates("sub_task")
    def validate_subtask(self, value):

        if value not in self.enabled_subtasks:
            raise ValidationError(
                {"sub_task": f"Subtask {value} is not configured"})


class EndpointDefinitionSchema(Schema):
    """Defines an endpoint uri and auth for a service.
    """
    class Meta:
        unknown = RAISE

    def __init__(self,
                 service: Type[ServiceDefinitionSchema],
                 allowed_schemes: List = ["https"]) -> None:

        self.service = service
        self._declared_fields["uri"] = fields.Url(
            require_tld=False,
            required=True,
            schemes=allowed_schemes,
            relative=False,
            help="endpoint URI"
        )

        self._declared_fields["backend"] = fields.Str(
            required=False,
            validate=self.backend_validator,
            missing=service.default_backend,
            default=service.default_backend,
            help="backend must be one of"
                 f"{str(self.service.allowed_backends)}"
        )

        super().__init__()

    auth = PolyField(
        serialization_schema_selector=serialize_auth,
        deserialization_schema_selector=deserialize_auth,
        required=False,
        allow_none=True,
        help="authentication object keyed on auth_type"
    )

    @validates_schema
    def endpoints_url_validator(self, data, **kwargs):
        try:
            try:
                regex = self.service.endpoint_regexes[data["backend"]]
            except KeyError:
                regex = self.service.endpoint_regexes["default"]
        except KeyError as e:
            key = str(e).strip("'")
            raise ValidationError({"uri": "No uri regex rule found for "
                                          f"backend {key}"})

        if not regex.fullmatch(data["uri"]):
            raise ValidationError(
                {"uri": f"url {data['uri']} does not match allowed urls"}
            )

    def backend_validator(self, value):
        if (self.service.allowed_backends
                and value not in self.service.allowed_backends):
            raise ValidationError(
                f'invalid value "{value}" for field "backend"'
                f" should be one of {str(self.service.allowed_backends)}")


service_types = {"artifact_store": ArtifactStoreSchema,
                 "sub_task": PiperCITaskSchema,
                 "task_manager": TaskManagerServiceSchema}
