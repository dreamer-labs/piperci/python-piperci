#!/bin/bash

dest_dir=$1

if ! [[ -f "${dest_dir}/minio" ]];
then
  if [[ "$(uname -s)" == 'Darwin' ]];
  then
    curl -o ${dest_dir}/minio "https://dl.min.io/server/minio/release/darwin-amd64/archive/minio.RELEASE.2019-07-10T00-34-56Z"
  elif [[ "$(uname -s)" == 'Linux' ]];
  then
    curl -o ${dest_dir}/minio "https://dl.min.io/server/minio/release/linux-amd64/archive/minio.RELEASE.2019-07-10T00-34-56Z"
  else
    echo "Unsupported platform $(uname -S)"
    exit 1
  fi
fi

hash="$(openssl dgst -sha256 -r ${dest_dir}/minio | cut -f1 -d' ')"

case $hash in
  '5d3e35ccdb759b38eabf2cd534a7311e76b2d521a9c76d04cd22df2c74c1a652'|'eb8d078a2787423dcb83ca516d5e7af194a2a8063086c435e0467ce53351c824')
    echo 'minio hash matched'
    chmod u+x ${dest_dir}/minio
    ;;
  *)
    echo 'Wrong or bad minio binary... or sha256 hash needs to be updated for new version'
    rm ${dest_dir}/minio
    exit 2
esac
