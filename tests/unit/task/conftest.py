import pytest


@pytest.fixture(scope="session")
def log_directory_fixture(tmpdir_factory):
    logdir = tmpdir_factory.mktemp("logs")
    for i in range(0, 5):
        log_file = logdir.join(f"log_file_{i}.txt")
        log_file.write("logs")
    return str(logdir)


@pytest.fixture(scope="session")
def log_file_fixture(tmpdir_factory):
    log_file = tmpdir_factory.mktemp("logs").join("log_file.txt")
    log_file.write("logs")
    return str(log_file)


@pytest.fixture
def request_new_task_patch(mocker):
    mocker.patch("piperci.gman.client.GmanClient.new_task")


@pytest.fixture
def requests_post_patch(mocker):
    mock_post = mocker.patch("piperci.task.this_task.requests.post")
    mock_post.status_code = 200


@pytest.fixture
def upload_file_patch_to_noop(mocker):
    mocker.patch("piperci.storeman.minio_client.MinioClient.upload_file")


@pytest.fixture
def download_file_patch(mocker):
    mocker.patch("piperci.storeman.minio_client.MinioClient.download_file")


@pytest.fixture
def artman_patch(mocker):
    mocker.patch("piperci.artman.client.ArtmanClient.create_artifact")


@pytest.fixture
def update_task_patch(mocker):
    mocker.patch("piperci.gman.client.GmanClient.update_task")


@pytest.fixture
def storage_fixture():
    storage = {
        "service": "artifact_store",
        "endpoints": [{"backend": "minio",
                       "uri": "https://localhost:6000",
                       "auth": {
                           "auth_type": "basic",
                           "username": "1234",
                           "password": "1234"
                       }}]}
    return storage


@pytest.fixture
def task_manager_fixture():
    return {
        "service": "task_manager",
        "endpoints": [{"backend": "gman",
                       "uri": "http://localhost:8089",
                       "auth": {
                           "auth_type": "basic",
                           "username": "1234",
                           "password": "1234"
                       }}]}


@pytest.fixture
def artifact():
    data = {
        "status": "unknown",
        "uri": "minio://someminio.example.com/art1",
        "artifact_id": "884053a3-277b-45e4-9813-fc61c07a2cd6",
        "type": "artifact",
        "sri": "sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=",
        "task": {
            "task_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
            "caller": "test_case_create_1",
            "project": "gman_test_data",
            "thread_id": "a9a1ca15-747d-43f9-8f04-1a66de8fef33",
            "run_id": "create_1",
        },
        "event_id": "a48efe28-db9e-4330-93c4-5f480b2bef71",
    }
    return [data]


@pytest.fixture
def task_response():
    return {
        "task": {
            "project": "test",
            "task_id": "1234",
            "caller": "python",
            "thread_id": "1234",
            "run_id": "1234",
        },
        "timestamp": "2019-06-07T16:58:20.513731+00:00",
        "message": "Requesting new taskID",
        "status": "completed",
    }
