import base64
import json
import pytest
import requests
import responses

from piperci.marshaller.task_manager import TaskManagerCreateArtifact

from marshmallow import ValidationError
from piperci.artman.client import ArtmanClient

_create_artifact_data = {
    "uri": "https://someminio.example.com/art1",
    "sri": "sha256-sCDaaxdshXhK4sA/v4dMHiMWhtGyQwA1fP8PgrN0O5g=",
    "type": "artifact",
    "task_id": "c95953c4-9702-4f0c-a1e8-317ba4ba7c6f",
}


def artifact_manager_response_callback(introspect,
                                       response_body,
                                       response_code=200,
                                       schema_type="create",
                                       auth=None):

    def callback(request):
        if auth:
            if isinstance(auth, requests.auth.AuthBase):
                creds = base64.b64decode(
                    request.headers["Authorization"].split(" ")[1]
                ).decode().split(":")
                assert auth.username == creds[0] and auth.password == creds[1]
        else:
            assert "Authorization" not in request.headers

        try:
            if schema_type == "create":
                body = json.loads(request.body)
                introspect["json"] = body
                TaskManagerCreateArtifact().load(body)
                introspect["marshaller"] = True
            elif schema_type == "get":
                introspect["marshaller"] = None

            return response_code, {}, json.dumps(response_body)
        except ValidationError as e:
            introspect["marshaller"] = False
            return 422, {}, json.dumps({"errors": e.messages})
        except Exception as e:
            introspect["marshaller"] = e
            return (400, {}, json.dumps({"errors": [{
                "conftest:conftest_gman_response":
                    f"Unknown error processing function"}]})
                    )
    return callback


def test_init_auth_fails():
    with pytest.raises(TypeError):
        ArtmanClient("http://artman", auth=("auth", "auth"))


def test_init_auth_unsupported_type():
    with pytest.raises(ValueError):
        ArtmanClient("http://artman", auth={"auth_type": "DNE"})


def test_init_auth_no_auth_type():
    with pytest.raises(ValueError):
        ArtmanClient("http://artman", auth={"no_valid_key": "DNE"})


def test_init_auth_passes_requests_auth_object():
    art_cli = ArtmanClient(
        "http://artman_url",
        auth=requests.auth.HTTPBasicAuth("user", "password")
    )

    assert isinstance(art_cli.auth, requests.auth.AuthBase)


@responses.activate
def test_create_artifact(create_artifact_response, artman_client):

    introspect = {}
    callback = artifact_manager_response_callback(introspect,
                                                  create_artifact_response,
                                                  auth=artman_client.auth)

    responses.add_callback(
        responses.POST, "http://artman_url/artifact",
        callback=callback
    )

    results = artman_client.create_artifact(**_create_artifact_data)

    assert results == create_artifact_response


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_create_artifact_bad_response_code(response_code, artman_client):
    introspect = {"json": {}}
    callback = artifact_manager_response_callback(introspect,
                                                  {},
                                                  response_code=response_code,
                                                  auth=artman_client.auth)

    responses.add_callback(
        responses.POST, "http://artman_url/artifact",
        callback=callback
    )

    with pytest.raises(requests.exceptions.RequestException):
        artman_client.create_artifact(**_create_artifact_data)


def test_create_artifact_request_exception(mock_post_request_exception, artman_client):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client.create_artifact(**_create_artifact_data)


@pytest.mark.parametrize("test", [(200, True), (404, False)])
@responses.activate
def test_check_artifact_exists_for_sri_exists(test, artman_client):
    responses.add(responses.HEAD, "http://artman_url/artifact/sri/1234", status=test[0])
    assert (
        artman_client._check_artifact_exists_for_sri(
            sri_urlsafe="1234"
        )
        == test[1]
    )


@responses.activate
def test_check_artifact_exists_for_sri_invalid_response(artman_client):
    responses.add(responses.HEAD, "http://artman_url/artifact/sri/1234", status=403)
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._check_artifact_exists_for_sri(
            sri_urlsafe="1234"
        )


def test_check_artifact_exists_for_sri_exception(
    mock_head_request_exception, artman_client
):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._check_artifact_exists_for_sri(
            sri_urlsafe="1234"
        )


@pytest.mark.parametrize("test", [(200, True), (404, False)])
@responses.activate
def test_check_artifact_exists_for_task_id_exists(test, artman_client):
    responses.add(
        responses.HEAD, "http://artman_url/artifact/task/1234", status=test[0]
    )
    assert (
        artman_client._check_artifact_exists_for_task_id(
            task_id="1234"
        )
        == test[1]
    )


@responses.activate
def test_check_artifact_exists_for_task_id_invalid_response(artman_client):
    responses.add(responses.HEAD, "http://artman_url/artifact/task/1234", status=403)
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._check_artifact_exists_for_task_id(
            task_id="1234"
        )


def test_check_artifact_exists_for_task_id_exception(
    mock_head_request_exception, artman_client
):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._check_artifact_exists_for_task_id(
            task_id="1234"
        )


@pytest.mark.parametrize(
    "arg",
    [
        (
            {"task_id": "1234"},
            "task/1234",
            {"x-gman-artifacts": "2"},
        ),
        (
            {"sri_urlsafe": "1234"},
            "sri/1234",
            {"x-gman-artifacts": "2"},
        ),
    ],
)
@responses.activate
def test_check_artifact_exists(arg, artman_client):
    responses.add(
        responses.HEAD, f"http://artman_url/artifact/{arg[1]}", headers=arg[2]
    )
    assert artman_client.check_artifact_exists(**arg[0])
    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == f"http://artman_url/artifact/{arg[1]}"


@pytest.mark.parametrize(
    "arg",
    [
        {"task_id": "1234", "sri_urlsafe": "1234"},
        {},
    ],
)
@responses.activate
def test_check_artifact_exists_invalid_parameter(arg, artman_client):
    with pytest.raises(ValueError):
        artman_client.check_artifact_exists(**arg)


@responses.activate
def test_check_artifact_status(artman_client):
    responses.add(
        responses.HEAD,
        "http://artman_url/artifact/1234",
        headers={"x-gman-artifact-status": "unknown"},
    )
    response = artman_client.check_artifact_status(
        artifact_id="1234"
    )

    assert response == "unknown"


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_check_artifact_status_bad_response_code(response_code, artman_client):
    responses.add(
        responses.HEAD, "http://artman_url/artifact/1234", status=response_code
    )
    with pytest.raises(requests.exceptions.HTTPError):
        artman_client.check_artifact_status(
            artifact_id="1234"
        )


def test_check_artifact_status_exception(mock_head_request_exception, artman_client):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client.check_artifact_status(
            artifact_id="1234"
        )


@responses.activate
def test_get_artifacts_by_sri(artifact_list, artman_client):
    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifact_list,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/sri/1234",
        callback=callback
    )

    response = artman_client._get_artifacts_by_sri(
        sri_urlsafe="1234"
    )

    assert len(response) == 2


@responses.activate
def test_get_artifacts_by_sri_query_filter(artifact_list, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifact_list,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/sri/1234",
        callback=callback
    )

    response = artman_client._get_artifacts_by_sri(
        sri_urlsafe="1234",
        query_filter=lambda x: x.get("status") == "unknown",
    )
    assert len(response) == 2


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_artifacts_by_sri_bad_response_code(response_code, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        "",
        response_code=response_code,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/sri/1234",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        artman_client._get_artifacts_by_sri(
            sri_urlsafe="1234"
        )


def test_get_artifacts_by_sri_request_exception(
    mock_get_request_exception, artman_client
):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._get_artifacts_by_sri(
            sri_urlsafe="1234"
        )


@responses.activate
def test_get_artifact_by_artifact_id(artifacts, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifacts[0],
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/1234",
        callback=callback
    )

    artman_client._get_artifact_by_artifact_id(
        artifact_id="1234"
    )

    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == "http://artman_url/artifact/1234"


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_artifact_by_artifact_id_bad_response_code(response_code, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        "",
        response_code=response_code,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/1234",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        artman_client._get_artifact_by_artifact_id(
            artifact_id="1234"
        )


def test_get_artifact_by_artifact_id_request_exception(
    mock_get_request_exception, artman_client
):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._get_artifact_by_artifact_id(
            artifact_id="1234"
        )


@responses.activate
def test_get_artifact_by_task_id(artifacts, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifacts,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/task/1234",
        callback=callback
    )

    artman_client._get_artifacts_by_task_id(
        task_id="1234"
    )
    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == "http://artman_url/artifact/task/1234"


@responses.activate
def test_get_artifacts_by_task_id_query_filter(artifact_list, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifact_list,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/task/1234",
        callback=callback
    )

    response = artman_client._get_artifacts_by_task_id(
        task_id="1234",
        query_filter=lambda x: x.get("status") == "unknown",
    )
    assert len(response) == 2


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_artifacts_by_task_id_bad_response_code(response_code, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        "",
        response_code=response_code,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/task/1234",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        artman_client._get_artifacts_by_task_id(
            task_id="1234"
        )


def test_get_artifacts_by_task_id_request_exception(
    mock_get_request_exception, artman_client
):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._get_artifacts_by_task_id(
            task_id="1234"
        )


@responses.activate
def test_get_artifact_by_thread_id(
    mocker, artifact_list,
    thread_id_tasks_fixture, gman_client, artman_client
):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifact_list,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/task/1234",
        callback=callback
    )

    mocker.patch(
        "piperci.gman.client.GmanClient.get_thread_tasks",
        return_value=thread_id_tasks_fixture
    )
    artman_client._get_artifacts_by_thread_id(
        gman_client=gman_client, thread_id="1234"
    )
    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == "http://artman_url/artifact/task/1234"


@responses.activate
def test_get_artifacts_by_thread_id_query_filter(
    mocker, artifact_list, thread_id_tasks_fixture, gman_client, artman_client
):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifact_list,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/task/1234",
        callback=callback
    )

    mocker.patch(
        "piperci.gman.client.GmanClient.get_thread_tasks",
        return_value=thread_id_tasks_fixture
    )
    response = artman_client._get_artifacts_by_thread_id(
        gman_client=gman_client,
        thread_id="1234",
        query_filter=lambda x: x.get("status") == "unknown",
    )
    assert len(response) == 2


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_artifacts_by_thread_id_bad_response_code(
    mocker, response_code, thread_id_tasks_fixture, gman_client, artman_client
):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        {},
        response_code=response_code,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/task/1234",
        callback=callback
    )

    mocker.patch(
        "piperci.gman.client.GmanClient.get_thread_tasks",
        return_value=thread_id_tasks_fixture
    )
    with pytest.raises(requests.exceptions.HTTPError):
        artman_client._get_artifacts_by_thread_id(
            gman_client=gman_client, thread_id="1234"
        )


def test_get_artifacts_by_thread_id_request_exception(
    mock_get_request_exception,
    gman_client, artman_client
):
    with pytest.raises(requests.exceptions.RequestException):
        artman_client._get_artifacts_by_thread_id(
            gman_client=gman_client, thread_id="1234"
        )


@pytest.mark.parametrize(
    "arg",
    [
        ({"task_id": "1234"}, "task/1234"),
        ({"sri_urlsafe": "1234"}, "sri/1234"),
        ({"artifact_id": "1234"}, "1234"),
    ],
)
@responses.activate
def test_get_artifact(arg, artifact_list, gman_client, artman_client):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifact_list,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, f"http://artman_url/artifact/{arg[1]}",
        callback=callback
    )

    artman_client.get_artifact(**arg[0], gman_client=gman_client)
    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == f"http://artman_url/artifact/{arg[1]}"


@responses.activate
def test_get_artifact_thread(
    mocker, artifact_list, thread_id_tasks_fixture, gman_client, artman_client
):

    introspect = {}

    callback = artifact_manager_response_callback(
        introspect,
        artifact_list,
        response_code=200,
        schema_type="get",
        auth=artman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://artman_url/artifact/task/1234",
        callback=callback
    )

    mocker.patch(
        "piperci.gman.client.GmanClient.get_thread_tasks",
        return_value=thread_id_tasks_fixture
    )
    artman_client.get_artifact(
        gman_client=gman_client,
        thread_id="1234"
    )
    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == "http://artman_url/artifact/task/1234"


@pytest.mark.parametrize(
    "arg",
    [
        {"task_id": "1234", "sri_urlsafe": "1234"},
        {"thread_id": "1234", "sri_urlsafe": "1234"},
        {"thread_id": "1234", "task_id": "1234"},
        {
            "task_id": "1234",
            "sri_urlsafe": "1234",
            "artifact_id": "1234",
        },
        {
            "artifact_id": "1234",
            "sri_urlsafe": "1234",
            "thread_id": "1234",
        },
        {
            "artifact_id": "1234",
            "task_id": "1234",
            "thread_id": "1234",
        },
        {
            "sri_urlsafe": "1234",
            "task_id": "1234",
            "thread_id": "1234",
        },
        {},
    ],
)
@responses.activate
def test_get_artifact_invalid_parameter(arg, gman_client, artman_client):
    with pytest.raises(ValueError):
        artman_client.get_artifact(**arg, gman_client=gman_client)
