import pytest

from marshmallow import ValidationError

from piperci.marshaller.auth import auth_types
from piperci.marshaller.config_schema import CustomSchema
from piperci.marshaller.task import TaskInitSchema


@pytest.mark.parametrize("auth_type", auth_types.keys())
def test_auth_schemas(auth_type, valid_auths):

    auth_data = auth_types[auth_type]().load(valid_auths[auth_type])

    assert auth_data == valid_auths[auth_type]


def test_load_bad_auth_type(valid_task):

    task_schema = TaskInitSchema(CustomSchema, ".*")

    (valid_task["service_configs"][0]
        ["endpoints"][0]["auth"]["auth_type"]) = "dne"

    with pytest.raises(ValidationError) as err:
        task_schema.load(valid_task)

    assert "invalid value dne for field" in str(err)


def test_load_missing_auth_type(valid_task):

    task_schema = TaskInitSchema(CustomSchema, ".*")

    del (valid_task["service_configs"][0]
         ["endpoints"][0]["auth"]["auth_type"])

    with pytest.raises(ValidationError) as err:
        task_schema.load(valid_task)

    assert 'missing "auth_type" field' in str(err)


def test_dump_missing_auth_type(valid_task):

    task_schema = TaskInitSchema(CustomSchema, ".*")

    del (valid_task["service_configs"][0]
         ["endpoints"][0]["auth"]["auth_type"])

    with pytest.raises(TypeError) as err:
        task_schema.dumps(valid_task)

    assert 'Error: missing "auth_type" field' in str(err)


def test_dump_invalid_auth_type(valid_task):

    task_schema = TaskInitSchema(CustomSchema, ".*")

    (valid_task["service_configs"][0]
        ["endpoints"][0]["auth"]["auth_type"]) = "dne"

    with pytest.raises(TypeError) as err:
        task_schema.dumps(valid_task)

    assert 'Error: invalid value dne for field "auth_typ' in str(err)
