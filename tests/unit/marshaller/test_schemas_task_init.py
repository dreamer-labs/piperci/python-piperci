import json
import pytest

from marshmallow import ValidationError

from piperci.marshaller.task import TaskInitSchema
from piperci.marshaller.config_schema import CustomSchema


class Delete:
    pass


class Modify:

    def __init__(self, value):
        self.value = value

    def __call__(self):
        return self.value


def test_load_valid(valid_task, enabled_subtasks):
    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)
    parsed = task_schema.load(valid_task)

    assert "service_configs" in parsed
    assert parsed["only_validate"] is False


@pytest.mark.parametrize("data_mods", [
    ["project", Delete],
    ["run_id", Delete],
    ["stage", Delete],
    ["service_configs", Delete],
    ["service_configs", Modify({"dict": "invalid_type"})],
    ["thread_id", Modify("abc2123notauuid")],
    ["parent_id", Modify("abc2123notauuid")],
    ["config", Modify("string type invalid")],
    ["config", Modify(["list", "type", "invalid"])],
    ["artifacts", Modify("abc123 invalid not a list")],
    ["artifacts", Modify(["abc123badsri"])]
])
def test_load_bad_top_level(valid_task, data_mods, enabled_subtasks):
    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)

    if data_mods[1] is Delete:
        del valid_task[data_mods[0]]
    elif isinstance(data_mods[1], Modify):
        valid_task[data_mods[0]] = data_mods[1]()

    with pytest.raises(ValidationError):
        task_schema.load(valid_task)


def test_valid_dump(valid_task, enabled_subtasks):

    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)

    json_str = task_schema.dumps(valid_task)
    check_dict = json.loads(json_str)
    assert "service_configs" in check_dict
