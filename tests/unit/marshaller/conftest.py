import pytest


@pytest.fixture
def valid_auths():
    return {"basic": {"auth_type": "basic",
                      "username": "bob",
                      "password": "A#*ul;:O*7y924$53"},
            "bearer": {"auth_type": "bearer",
                       "token": "A#*ul;:O*7y924$53"}}


@pytest.fixture
def valid_services():
    return {"artifact_store": {"service": "artifact_store",
                               "endpoints": [{"backend": "minio",
                                              "uri": "https://localhost:6000",
                                              "auth": None}]},
            "task_manager": {"service": "task_manager",
                             "endpoints": [{"backend": "gman",
                                            "uri": "https://localhost:6002",
                                            "auth": None}]},
            "sub_task": {"service": "sub_task",
                         "sub_task": "test_subtask",
                         "endpoints": [{"backend": "piperci",
                                        "uri": "https://localhost:6001",
                                        "auth": None}]}}


@pytest.fixture
def enabled_subtasks():
    return ["test_subtask", "another_subtask"]


@pytest.fixture
def valid_task(valid_auths, valid_services):
    (valid_services["artifact_store"]
     ["endpoints"][0]["auth"]) = valid_auths["basic"]
    return {
        "project": "A Test Project",
        "run_id": "1234",
        "stage": "tox_tests",
        "thread_id": "5c96f41a-3614-4b0b-a83c-a700fc75d5b9",
        "task_id": "5c96f41a-3614-4b0b-a83c-a700fc75d5b9",
        "parent_id": "ed450ab7-38b2-425b-9bc6-c057ebc9b39a",
        "caller": "test_case_create_1",
        "config": {},
        "service_configs": [valid_services["artifact_store"]]}
