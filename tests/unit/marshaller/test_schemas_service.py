import re
import pytest
from types import TracebackType
from typing import cast, Optional, Tuple

from _pytest.python_api import RaisesContext, _E

from marshmallow import ValidationError

from piperci.marshaller.service import service_types
from piperci.marshaller.config_schema import CustomSchema
from piperci.marshaller.task import TaskInitSchema


@pytest.mark.parametrize("service", service_types.keys())
def test_service_schema_direct_load(service, valid_services, enabled_subtasks):
    service_data = service_types[service](
        endpoint_regexes=re.compile(".*"),
        enabled_subtasks=enabled_subtasks
    ).load(valid_services[service])

    assert service_data == valid_services[service]


def test_load_bad_service(valid_task, enabled_subtasks):
    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)
    valid_task["service_configs"][0]["service"] = "dne"

    with pytest.raises(ValidationError) as err:
        task_schema.load(valid_task)

    assert "invalid value dne for field" in str(err)


def test_load_default_endpoint_backend(valid_task, enabled_subtasks):
    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)
    Service = service_types[
        valid_task["service_configs"][0]["service"]]

    service = Service(".*")

    del (valid_task["service_configs"][0]
         ["endpoints"][0]["backend"])

    task_data = task_schema.load(valid_task)

    assert (task_data["service_configs"][0]
            ["endpoints"][0]["backend"]) == service.default_backend


class NotRaises(RaisesContext):

    def __exit__(
        self,
        exc_type: Optional["Type[BaseException]"],
        exc_val: Optional[BaseException],
        exc_tb: Optional[TracebackType],
    ) -> bool:

        __tracebackhide__ = True

        assert self.excinfo is not None
        if not exc_type:
            return True

        if exc_type and not issubclass(exc_type, self.expected_exception):
            return True

        exc_info = cast(
            Tuple["Type[_E]", _E, TracebackType], (exc_type, exc_val, exc_tb)
        )
        self.excinfo.fill_unfilled(exc_info)
        if self.match_expr is not None:
            self.excinfo.match(self.match_expr)
        return False


@pytest.mark.parametrize("force_https,uri,context", [
    [True, "https://localhost:6000", NotRaises],
    [False, "http://localhost:6000", NotRaises],
    [True, "http://localhost:6000", RaisesContext],
    [False, "https://localhost:6000", NotRaises]
])
def test_load_force_https(force_https, uri, context, valid_task,
                          enabled_subtasks):

    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 force_https=force_https,
                                 enabled_subtasks=enabled_subtasks)

    valid_task["service_configs"][0]["endpoints"][0]["uri"] = uri

    with context(ValidationError, "ValidationError was detected"):
        task_schema.load(valid_task)


@pytest.mark.parametrize("allowed,backend,context", [
    [["minio"], "minio", NotRaises],
    [["test", "new_thing"], "new_thing", NotRaises],
    [["test"], "bad_backend", RaisesContext],
    [None, "minio", NotRaises],
    [None, "bad_backend", RaisesContext]
])
def test_load_backends(allowed, backend, context, valid_task, enabled_subtasks):
    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 allowed_backends=allowed,
                                 enabled_subtasks=enabled_subtasks)

    (valid_task["service_configs"][0]
     ["endpoints"][0]["backend"]) = backend

    with context(ValidationError, "ValidationError was detected"):
        task_schema.load(valid_task)


@pytest.mark.parametrize("endpoint_regex", [
    ".*localhost:4000",
    re.compile(".*localhost:5000"),
    {"minio": re.compile(".*localhost:9000")},
    {"minio": ".*localhost:8000"},
    {"default": ".*localhost:8000"},
])
def test_load_uri_regex_validation(endpoint_regex, valid_task,
                                   enabled_subtasks):

    task_schema = TaskInitSchema(CustomSchema, endpoint_regex,
                                 enabled_subtasks=enabled_subtasks)

    with pytest.raises(ValidationError) as err:
        task_schema.load(valid_task)

    assert ('url https://localhost:6000 '
            'does not match allowed urls' in str(err))


def test_load_missing_uri_regex(valid_task, enabled_subtasks):
    # tests the case where a regex is defined for a gman uri
    # but not for the minio backend and no default is set
    task_schema = TaskInitSchema(CustomSchema, {"gman": ".*localhost:8000"})

    with pytest.raises(ValidationError) as err:
        task_schema.load(valid_task)

    assert "No uri regex rule found for backend" in str(err)


def test_load_missing_service(valid_task, enabled_subtasks):

    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)

    del valid_task["service_configs"][0]["service"]

    with pytest.raises(ValidationError) as err:
        task_schema.load(valid_task)

    assert 'missing "service" field' in str(err)


def test_dump_missing_service(valid_task, enabled_subtasks):

    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)

    del valid_task["service_configs"][0]["service"]

    with pytest.raises(TypeError) as err:
        task_schema.dumps(valid_task, enabled_subtasks)

    assert 'Error: missing "service" field' in str(err)


def test_dump_invalid_service(valid_task, enabled_subtasks):

    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)

    valid_task["service_configs"][0]["service"] = "dne"

    with pytest.raises(TypeError) as err:
        task_schema.dumps(valid_task)

    assert 'Error: invalid value dne for field "service"' in str(err)


@pytest.mark.parametrize("enabled_subtasks,context", [
    ["test_subtask", NotRaises],
    ["test_not_configured", RaisesContext]
])
def test_subtask_not_configured(enabled_subtasks,
                                context,
                                valid_task,
                                valid_services):

    valid_task["service_configs"].append(valid_services["sub_task"])
    task_schema = TaskInitSchema(CustomSchema, ".*",
                                 enabled_subtasks=enabled_subtasks)

    with context(ValidationError, "matched config exception"):
        task_schema.load(valid_task)
