import base64
import json
import logging
import uuid

import pytest
import requests
import responses

from piperci.gman.exceptions import TaskError
from piperci.marshaller.task_manager import TaskManagerCreateTask, TaskManagerUpdateTask
from piperci.gman.client import GmanClient

from marshmallow import ValidationError


def task_manager_response_callback(introspect,
                                   response_body,
                                   response_code=200,
                                   schema_type="create",
                                   auth=None):

    def callback(request):

        if auth:
            if isinstance(auth, requests.auth.AuthBase):
                creds = base64.b64decode(
                    request.headers["Authorization"].split(" ")[1]
                ).decode().split(":")
                assert auth.username == creds[0] and auth.password == creds[1]
        else:
            assert "Authorization" not in request.headers

        try:
            if schema_type == "create":
                body = json.loads(request.body)
                introspect["json"] = body
                TaskManagerCreateTask().load(body)
                introspect["marshaller"] = True
            elif schema_type == "update":
                body = json.loads(request.body)
                introspect["json"] = body
                TaskManagerUpdateTask().load(body)
                introspect["marshaller"] = True
            elif schema_type == "get":
                introspect["marshaller"] = None

            return response_code, {}, json.dumps(response_body)
        except ValidationError as e:
            introspect["marshaller"] = False
            return 422, {}, json.dumps({"errors": e.messages})
        except Exception as e:
            introspect["marshaller"] = e
            return (400, {}, json.dumps({"errors": [{
                "conftest:conftest_gman_response":
                    f"Unknown error processing function"}]})
                    )
    return callback


thread_uuid = str(uuid.uuid4())
artifact_uuid = str(uuid.uuid4())
parent_uuid = str(uuid.uuid4())


@responses.activate
def test_debug(caplog, gman_client):
    introspect = {"json": {}}

    callback = task_manager_response_callback(introspect,
                                              {"task": {"task_id": "1234"}},
                                              auth=gman_client.auth)

    with caplog.at_level(logging.DEBUG):
        responses.add_callback(
            responses.POST, "http://gman_url/task",
            callback=callback
        )

        gman_client.new_task(
            run_id="1234",
            project="tests",
            caller="test_debug",
            status="started",
        )
        assert "Requesting new taskID" in caplog.text


def test_init_auth_fails():
    with pytest.raises(TypeError):
        GmanClient("http://gman_url", auth=("auth", "auth"))


def test_init_auth_unsupported_type():
    with pytest.raises(ValueError):
        GmanClient("http://gman_url", auth={"auth_type": "DNE"})


def test_init_auth_no_auth_type():
    with pytest.raises(ValueError):
        GmanClient("http://gman_url", auth={"no_valid_key": "DNE"})


@responses.activate
def test_new_task(gman_client):

    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              {"task": {"task_id": "1234"}},
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.POST, "http://gman_url/task",
        callback=callback
    )

    resp = gman_client.new_task(
        run_id="1234",
        project="tests",
        caller="test_debug",
        status="started",
    )
    assert resp == {"task": {"task_id": "1234"}}


@responses.activate
def test_new_task_with_thread_id(gman_client):
    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              {"task": {"task_id": "1234"}},
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.POST, "http://gman_url/task",
        callback=callback
    )
    resp = gman_client.new_task(
        run_id="1234",
        project="tests",
        caller="test_debug",
        status="started",
        thread_id=thread_uuid,
    )
    assert resp == {"task": {"task_id": "1234"}}


@responses.activate
def test_new_task_with_status_received(gman_client):
    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              {"task": {"task_id": "1234"}},
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.POST, "http://gman_url/task",
        callback=callback
    )
    resp = gman_client.new_task(
        run_id="1234",
        project="tests",
        status="received",
        caller="test_new_task_with_status_received",
        thread_id=thread_uuid,
        parent_id=parent_uuid,
    )

    assert b"parent_id" in responses.calls[0].request.body
    assert b"thread_id" in responses.calls[0].request.body
    assert resp == {"task": {"task_id": "1234"}}


def test_new_task_invalid_status(mock_post_request_exception, gman_client):
    with pytest.raises(ValueError):

        gman_client.new_task(
            run_id="1234",
            project="tests",
            caller="test_debug",
            status="invalid",
        )


def test_new_task_no_status(mock_post_request_exception, gman_client):
    with pytest.raises(TypeError):
        gman_client.new_task(
            run_id="1234",
            project="tests",
            caller="test_debug",
        )


def test_new_task_no_thread_id_with_received_status(
    mock_post_request_exception,
    gman_client
):
    with pytest.raises(ValueError):

        gman_client.new_task(
            run_id="1234",
            project="tests",
            caller="test_debug",
            status="received",
        )


def test_new_task_no_parent_id_with_received_status(gman_client):
    with pytest.raises(ValueError):
        gman_client.new_task(
            run_id="1234",
            project="tests",
            caller="test_debug",
            status="received",
            thread_id=thread_uuid,
        )


def test_request_new_task_no_thread_id_with_parent_id_with_received_status(gman_client):
    with pytest.raises(ValueError):
        gman_client.new_task(
            run_id="1234",
            project="tests",
            caller="test_new_task_no_thread",
            status="received",
            parent_id="1234",
        )


def test_new_task_exception(gman_client):
    with pytest.raises(requests.exceptions.RequestException):

        gman_client.new_task(
            run_id="1234",
            project="tests",
            caller="test_new_task_exception",
            status="started",
        )


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_new_task_failure(response_code, gman_client):

    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              {"task": {"task_id": "1234"}},
                                              response_code=response_code,
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.POST, "http://gman_url/task",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        gman_client.new_task(
            run_id="1234",
            project="test",
            caller="test_new_task_failure",
            status="started",
        )


@responses.activate
def test_wait_for_task_id(task_event_list, gman_client):
    introspect = {}
    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )

    resp = gman_client.wait_for_task(
        task_id="1234", status="completed"
    )

    assert resp


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_wait_for_task_id_fails_request(response_code, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        {},
        response_code=response_code,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )
    with pytest.raises(requests.exceptions.HTTPError):
        gman_client.wait_for_task(
            task_id="1234", status="completed"
        )


def test_wait_for_task_id_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.wait_for_task(
            task_id="1234", status="running", retry_max=2
        )


@responses.activate
def test_wait_for_task_id_times_out(task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )

    with pytest.raises(TimeoutError):
        gman_client.wait_for_task(
            task_id="1234", status="running", retry_max=2
        )


@responses.activate
def test_wait_for_task_id_task_fails(task_event_list_failures, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list_failures,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )

    with pytest.raises(TaskError):
        gman_client.wait_for_task(
            task_id="1234", status="running", retry_max=2
        )


@responses.activate
def test_update_task_artifact(gman_client):
    task_event = {
        "message": "blank message",
        "status": "running",
        "thread_id": thread_uuid,
        "timestamp": "2019-05-16T19:56:33.231452+00:00",
        "task": {
            "project": "python_project",
            "run_id": "574b1db2-ae55-41bb-8680-43703f3031f2",
            "caller": "gateway",
            "task_id": "1234",
        },
    }

    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              task_event, schema_type="update",
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.PUT,
        "http://gman_url/task/1234",
        callback=callback
    )

    gman_client.update_task(
        task_id="1234",
        status="running",
        message="blank message",
        artifact_id=artifact_uuid,
    )
    assert b"artifact" in responses.calls[0].request.body


@responses.activate
def test_update_task_return_code(gman_client):
    task_event = {
        "message": "blank message",
        "status": "running",
        "thread_id": thread_uuid,
        "timestamp": "2019-05-16T19:56:33.231452+00:00",
        "task": {
            "project": "python_project",
            "run_id": "574b1db2-ae55-41bb-8680-43703f3031f2",
            "caller": "gateway",
            "task_id": "1234",
        },
    }

    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              task_event, schema_type="update",
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.PUT, "http://gman_url/task/1234",
        callback=callback
    )

    gman_client.update_task(
        task_id="1234",
        status="running",
        message="blank message",
        return_code=1,
    )

    assert b"return_code" in responses.calls[0].request.body


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_update_task_fails_request(response_code, gman_client):

    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              {},
                                              response_code=response_code,
                                              schema_type="update",
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.PUT, "http://gman_url/task/1234",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        gman_client.update_task(
            task_id="1234",
            status="running",
            message="blank message",
        )


def test_update_task_request_exception(mock_put_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.update_task(
            task_id="1234",
            status="running",
            message="blank message",
        )


@responses.activate
def test_get_task_events(task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )

    resp = gman_client._get_task_events(task_id="1234")

    assert resp == task_event_list


@responses.activate
def test_get_task_events_query_filter_returns_none(task_event_list,
                                                   gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )

    resp = gman_client._get_task_events(
        task_id="1234",
        query_filter=lambda x: x.get("status") == "delegated",
    )
    assert not len(resp)


@responses.activate
def test_get_task_events_returns_one_item(task_event_list, gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )

    resp = gman_client._get_task_events(
        task_id="1234",
        query_filter=lambda x: x.get("status") == "completed",
    )
    assert len(resp) == 1


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_task_events_fails_request(response_code, gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        "",
        response_code=response_code,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234/events",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        gman_client._get_task_events(task_id="1234")


def test_get_task_events_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client._get_task_events(task_id="1234")


@responses.activate
def test_get_thread_tasks(task_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234",
        callback=callback
    )

    resp = gman_client.get_thread_tasks(thread_id="1234")
    assert len(resp)


@responses.activate
def test_get_thread_tasks_query_filter_returns_one(task_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234",
        callback=callback
    )

    resp = gman_client.get_thread_tasks(
        thread_id="1234",
        query_filter=lambda x: x.get("caller") == "gateway",
    )
    assert len(resp) == 1


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_thread_tasks_fails_request(response_code, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        {},
        response_code=response_code,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        gman_client.get_thread_tasks(thread_id="1234")


def test_get_thread_tasks_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.get_thread_tasks(thread_id="1234")


@responses.activate
def test_get_thread_events(task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234/events",
        callback=callback
    )

    resp = gman_client._get_thread_events(thread_id="1234")
    assert resp


@responses.activate
def test_get_thread_events_returns_one_item(task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234/events",
        callback=callback
    )

    resp = gman_client._get_thread_events(
        thread_id="1234",
        query_filter=lambda x: x.get("status") == "completed",
    )
    assert len(resp) == 1


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_thread_events_fails_request(response_code, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        "",
        response_code=response_code,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234/events",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        gman_client._get_thread_events(thread_id="1234")


def test_get_thread_events_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client._get_thread_events(thread_id="1234")


@responses.activate
def test_wait_for_thread(gman_client):
    thread_status_headers = {
        "x-gman-tasks-running": "0",
        "x-gman-tasks-completed": "1",
        "x-gman-tasks-pending": "0",
        "x-gman-tasks-failed": "0",
    }

    responses.add(
        responses.HEAD, "http://gman_url/thread/1234", headers=thread_status_headers
    )
    resp = gman_client.wait_for_thread(
        thread_id="1234"
    )

    assert resp


@responses.activate
def test_wait_for_thread_has_failures(gman_client):
    thread_status_headers = {
        "x-gman-tasks-running": "0",
        "x-gman-tasks-completed": "1",
        "x-gman-tasks-failed": "1",
    }
    responses.add(
        responses.HEAD, "http://gman_url/thread/1234", headers=thread_status_headers
    )
    with pytest.raises(TaskError):
        gman_client.wait_for_thread(thread_id="1234")


@responses.activate
def test_wait_for_thread_times_out(gman_client):
    thread_status_headers = {
        "x-gman-tasks-running": "1",
        "x-gman-tasks-completed": "0",
        "x-gman-tasks-failed": "0",
    }
    responses.add(
        responses.HEAD, "http://gman_url/thread/1234", headers=thread_status_headers
    )
    with pytest.raises(TimeoutError):
        gman_client.wait_for_thread(
            thread_id="1234", retry_max=2
        )


@responses.activate
def test_wait_for_thread_times_out_on_pending(gman_client):
    thread_status_headers = {
        "x-gman-tasks-running": "0",
        "x-gman-tasks-completed": "1",
        "x-gman-tasks-pending": "1",
        "x-gman-tasks-failed": "0",
    }
    responses.add(
        responses.HEAD, "http://gman_url/thread/1234", headers=thread_status_headers
    )
    with pytest.raises(TimeoutError):
        gman_client.wait_for_thread(
            thread_id="1234", retry_max=2
        )


@responses.activate
def test_wait_for_thread_times_out_invalid_headers(gman_client):
    thread_status_headers = {
        "x-gman-tasks-running": "0",
        "x-gman-tasks-completed": "0",
        "x-gman-tasks-pending": "0",
        "x-gman-tasks-failed": "0",
    }
    responses.add(
        responses.HEAD, "http://gman_url/thread/1234",
        headers=thread_status_headers,
        json={}
    )
    with pytest.raises(TimeoutError):
        gman_client.wait_for_thread(
            thread_id="1234", retry_max=2
        )


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_wait_for_thread_fails_request(response_code, gman_client):
    responses.add(responses.HEAD, "http://gman_url/thread/1234", status=response_code)
    with pytest.raises(requests.exceptions.HTTPError):
        gman_client.wait_for_thread(thread_id="1234")


def test_wait_for_thread_request_exception(mock_head_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.wait_for_thread(thread_id="1234")


@responses.activate
def test_get_run_events(task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/run/1234/events",
        callback=callback
    )

    resp = gman_client._get_run_events(run_id="1234")
    assert resp == task_event_list


@responses.activate
def test_get_run_events_query_filter_returns_none(task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        "",
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/run/1234/events",
        callback=callback
    )

    resp = gman_client._get_run_events(
        run_id="1234",
        query_filter=lambda x: x.get("status") == "delegated",
    )
    assert not len(resp)


@responses.activate
def test_get_run_events_returns_one_item(task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/run/1234/events",
        callback=callback
    )

    resp = gman_client._get_run_events(
        run_id="1234",
        query_filter=lambda x: x.get("status") == "completed",
    )
    assert len(resp) == 1


@pytest.mark.parametrize(
    "arg",
    [
        ({"task_id": "1234"}, "task/1234/events"),
        ({"thread_id": "1234"}, "thread/1234/events"),
        ({"run_id": "1234"}, "run/1234/events"),
    ],
)
@responses.activate
def test_get_events(arg, task_event_list, gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        task_event_list,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, f"http://gman_url/{arg[1]}",
        callback=callback
    )

    gman_client.get_events(**arg[0])
    assert len(responses.calls) == 1
    assert responses.calls[0].request.url == f"http://gman_url/{arg[1]}"


@pytest.mark.parametrize(
    "arg",
    [
        {"task_id": "1234", "thread_id": "1234"},
        {"thread_id": "1234", "run_id": "1234"},
        {"task_id": "1234", "run_id": "1234"},
        {},
    ],
)
@responses.activate
def test_get_events_invalid_parameter(arg, gman_client):
    with pytest.raises(ValueError):
        gman_client.get_events(**arg)


@pytest.mark.parametrize("response_code", [400, 500])
@responses.activate
def test_get_run_events_fails_request(response_code, gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        "",
        response_code=response_code,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/run/1234/events",
        callback=callback
    )

    with pytest.raises(requests.exceptions.HTTPError):
        gman_client._get_run_events(run_id="1234")


def test_get_run_events_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client._get_run_events(run_id="1234")


@responses.activate
def test_request_new_task_artifact(gman_client):
    introspect = {"json": {}}
    callback = task_manager_response_callback(introspect,
                                              {"task": {"task_id": "1234"}},
                                              auth=gman_client.auth)

    responses.add_callback(
        responses.POST, "http://gman_url/task",
        callback=callback
    )

    gman_client.new_task(
        run_id="1234",
        project="tests",
        caller="test_debug",
        status="started",
    )


@responses.activate
def test_get_task(gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        {"task": {"task_id": "1234"}},
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234",
        callback=callback
    )

    task = gman_client.get_task(
        task_id="1234"
    )

    assert task


@responses.activate
def test_get_task_returns_empty(gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        "",
        response_code=404,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/task/1234",
        callback=callback
    )

    task = gman_client.get_task(
        task_id="1234"
    )

    assert not task


def test_get_get_task_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.get_task(task_id="1234")


@responses.activate
def test_get_run_id(gman_client):
    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        [{"run_id": "1234"}],
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/run/1234",
        callback=callback
    )

    response = gman_client.get_run(run_id="1234")

    assert response


@responses.activate
def test_get_run_id_empty(gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        "",
        response_code=404,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/run/1234",
        callback=callback
    )

    response = gman_client.get_run(run_id="1234")

    assert not response


def test_get_run_id_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.get_run(run_id="1234")


@responses.activate
def test_get_thread_id(gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        [{"thread_id": "1234"}],
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234",
        callback=callback
    )

    response = gman_client.get_thread(thread_id="1234")

    assert response


@responses.activate
def test_get_thread_id_empty(gman_client):

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        "",
        response_code=404,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/thread/1234",
        callback=callback
    )

    response = gman_client.get_thread(thread_id="1234")

    assert not response


def test_get_thread_id_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.get_thread(thread_id="1234")


@responses.activate
def test_get_health_returns_response(gman_client):
    status = {"status": "Healthy"}

    introspect = {}

    callback = task_manager_response_callback(
        introspect,
        status,
        response_code=200,
        schema_type="get",
        auth=gman_client.auth
    )

    responses.add_callback(
        responses.GET, "http://gman_url/health",
        callback=callback
    )

    response = gman_client.get_health()

    assert response == status


def test_get_health_request_exception(mock_get_request_exception, gman_client):
    with pytest.raises(requests.exceptions.RequestException):
        gman_client.get_health()
